#! /usr/bin/env python2
# -*- coding: utf-8 -*-
# dependency setup
# sudo pip install csv
# sudo pip install pyqt4

import os
import sys
import csv
import xmltodict
from urlparse import urlparse

from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QMessageBox

from alcyone.guialcyone import  GUIAlcyone

import gsheethelper
import uidropbox
from uisettings import UISettings


class UIMonkey(QtGui.QWidget):

    project_path = None

    settings = None
    permitted_uri = {}

    prev_selected_combo_items = {"Catagory": None, "URI": None}

    # UI
    uri_comboBox   = None
    catagory_comboBox = None
    dropbox        = None

    def __init__(self):
        self.project_path = os.getcwd()
        self.LoadSettings()

        QtGui.QWidget.__init__(self)

        # Set window size.
        self.resize(500, 220)
        self.setFixedHeight(220)

        # Set window title
        self.setWindowTitle("Monkey Fu")
        self.setWindowIcon(QtGui.QIcon("res/monkey-icon.png"))

        self.UiSetup()
        self.UpdateList()

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.close()

    def UiSetup(self):

        self.dropbox = uidropbox.UIDropbox("drop box", self)

        top_hbox = QtGui.QHBoxLayout()

        # combo box
        # self.policy = QtGui.QSizePolicy()
        # self.policy.setHorizontalStretch(3)
        # self.policy.setHorizontalPolicy(QtGui.QSizePolicy.Expanding)

        self.catagory_comboBox = QtGui.QComboBox(self)
        self.catagory_comboBox.currentIndexChanged.connect(self.OnCatagoryChange)
        # self.catagory_comboBox.setSizePolicy(self.policy)
        top_hbox.addWidget(self.catagory_comboBox)

        self.uri_comboBox = QtGui.QComboBox(self)
        self.uri_comboBox.currentIndexChanged.connect(self.OnURIChange)
        # self.uri_comboBox.setSizePolicy(self.policy)
        top_hbox.addWidget(self.uri_comboBox)

        self.refresh_button = QtGui.QPushButton(self)
        self.refresh_button.clicked.connect(self.RefreshData)
        self.refresh_button.setIcon(QtGui.QIcon("res/refresh-512.png"))
        self.refresh_button.setIconSize(QtCore.QSize(20, 20))
        self.refresh_button.setMaximumSize(QtCore.QSize(30, 30))
        top_hbox.addWidget(self.refresh_button)

        # action buttons
        bottom_hbox = QtGui.QHBoxLayout()

        self.settings_button = QtGui.QPushButton( self)
        self.settings_button.setIcon(QtGui.QIcon("res/Control-Panel.png"))
        self.settings_button.setIconSize(QtCore.QSize(20, 20))
        self.settings_button.setMaximumSize(QtCore.QSize(30, 30))
        self.settings_button.clicked.connect(self.OnSettingsOpen)
        bottom_hbox.addWidget(self.settings_button)

        self.next_button = QtGui.QPushButton("Push", self)
        self.next_button.clicked.connect(self.OnPush)
        bottom_hbox.addWidget(self.next_button)

        vbox = QtGui.QVBoxLayout(self)
        vbox.addWidget(self.dropbox,1)
        vbox.addLayout(top_hbox)
        vbox.addLayout(bottom_hbox)

    def InitCatagory(self):

        self.catagory_comboBox.clear()

        for uri in self.permitted_uri.keys():
            self.catagory_comboBox.addItem(uri)

        #set previous selection if found
        if self.prev_selected_combo_items["Catagory"] is not None:
            prev_selection_index = self.catagory_comboBox.findText(self.prev_selected_combo_items["Catagory"], QtCore.Qt.MatchFixedString)
            if prev_selection_index != -1:
                self.catagory_comboBox.setCurrentIndex(prev_selection_index)

        self.InitURI()

    def InitURI(self):

        self.uri_comboBox.clear()
        sheet = str(self.catagory_comboBox.currentText())

        if sheet == "":
            return

        for uri in self.permitted_uri[sheet]:
            self.uri_comboBox.addItem(uri)

        #set previous selection if found
        if self.prev_selected_combo_items["URI"] is not None:
            prev_selection_index = self.uri_comboBox.findText(self.prev_selected_combo_items["URI"], QtCore.Qt.MatchFixedString)
            if prev_selection_index != -1:
                self.uri_comboBox.setCurrentIndex(prev_selection_index)

    def OnPush(self):
        if self.dropbox.required_file == None:
            self.ShowWarningDialog("CDN selection is empty")
        elif self.dropbox.selected_file == None:
            self.ShowWarningDialog("You have not dropped a file")
        elif not self.dropbox.DoesFileNamesMatch():
            if self.ShowWarningDialog("File names don't match do you still want to proceed", True):
                self.pushFile()
        else:
            self.pushFile()

    def RefreshData(self):

        self.prev_selected_combo_items["URI"] = str(self.uri_comboBox.currentText())
        self.prev_selected_combo_items["Catagory"] = str(self.catagory_comboBox.currentText())

        self.UpdateList()
        print "Refreshed"

    def OnSettingsOpen(self):
        print "Opening settings"
        self.LoadSettings()
        self.settings_window = UISettings(settingdict=self.settings)
        self.settings_window.show()
        self.settings_window.raise_()

    def OnCatagoryChange(self, i):
        print "Sheet Selection changed ", self.catagory_comboBox.currentText()
        self.InitURI()

    def OnURIChange(self, i):
        print "URI Selection changed ", self.uri_comboBox.currentText()
        self.dropbox.SetRequiredFile(os.path.basename(str(self.uri_comboBox.currentText())))
        self.dropbox.UpdateUI()

    def LoadSettings(self):
        try:
            with open('settings.xml') as f:
                self.settings = xmltodict.parse(f.read())
        except:
            self.ShowWarningDialog("Unable to configure settings, check setting.xml")
            raise

    def UpdateList(self):
        self.FetchPermissionList()
        self.InitCatagory()

    def FetchPermissionList(self):
        print "Fetching permissions"
        try:
            data = gsheethelper.GSheet.GetSheet(self.settings["settings"]["gdocURI"], int(self.settings["settings"]["gsheetId"]))
        except:
            raise

        #check csv sanity
        try:
            dialect = csv.Sniffer().sniff(data,[',',';'])
        except:
            self.ShowWarningDialog("Fetched permit data not good, contact admin")
            self.permitted_uri = None
            return

        reader = csv.DictReader(data.splitlines())
        self.permitted_uri = self.GetSortedDict(reader=reader)

    def ShowWarningDialog(self,text,cancellable=False):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Warning)

        msg.setText(text)
        msg.setWindowTitle("Something is bananas!")
        if cancellable:
            msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
        else:
            msg.setStandardButtons(QMessageBox.Ok)

        retval = msg.exec_()
        if retval == QMessageBox.Ok:
            return True
        else:
            print "User cancelled it"
            return False

    def pushFile(self):
        self.RefreshData()
        if self.prev_selected_combo_items["Catagory"] is not None and self.catagory_comboBox.findText(self.prev_selected_combo_items["Catagory"], QtCore.Qt.MatchFixedString) == -1:
            self.ShowWarningDialog("Catagory not available on permitted list")
            return
        elif self.prev_selected_combo_items["URI"] is not None and self.uri_comboBox.findText(self.prev_selected_combo_items["URI"], QtCore.Qt.MatchFixedString) == -1:
            self.ShowWarningDialog("Path not available on permitted list")
            return

        parsed_uri = urlparse(str(self.uri_comboBox.currentText()))
        bucket_name = parsed_uri.netloc
        bucket_root = parsed_uri.path

        purge_achash = self.settings["settings"]["hwind"]["achash"]
        purge_token  = self.settings["settings"]["hwind"]["token"]

        if purge_achash == "-1":
            self.ShowWarningDialog("Purge account hash not set")
            return

        elif purge_token == "-1":
            self.ShowWarningDialog("Purge token not set")
            return



        file_path = [urlparse(self.dropbox.selected_file).path];
        truncate_path = os.path.dirname(file_path[0])


        print "Pushing"
        print "bucket_name: " +bucket_name
        print "bucket_root: " +bucket_root

        print "File path: "     +file_path[0]
        print "Truncate path: " +truncate_path

        GUIAlcyone(filepaths=file_path,truncate=truncate_path,bucketname=bucket_name,root=bucket_root,
                             purge=True, hwndacnthash=purge_achash, hwndtoken=purge_token,guitype=GUIAlcyone.GUIType_ALL)

    def GetSortedDict(self,reader):
        sorted_dict = {}
        for row in reader:
            # #empty row ignore it
            if row["Access"] == "" or row["Access"] == "Deny" or row["Catagory"] == "" or row["URI"] == "":
                continue

            if sorted_dict.has_key(row["Catagory"]):
                sorted_dict[row["Catagory"]].append(row["URI"])
            else:
                sorted_dict.setdefault(row["Catagory"], [row["URI"]])

        return sorted_dict


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    monkey_window = UIMonkey()
    monkey_window.show()
    sys.exit(app.exec_())


