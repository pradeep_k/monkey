from PyQt4 import QtCore
from PyQt4.QtGui import QWidget
from PyQt4.QtGui import QFormLayout
from PyQt4.QtGui import QLineEdit
from PyQt4.QtGui import QPushButton
from PyQt4.QtGui import QIntValidator


import dicttoxml

class UISettings(QWidget):

    gdoc_sheet_uri_ui = None
    gdoc_sheet_id_ui = None

    hwind_achash    = None
    hwind_token     = None

    def __init__(self, settingdict=None):

        QWidget.__init__(self)
        form = QFormLayout()

        # self.setFixedSize(500, 300)
        self.setWindowTitle("Settings")
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setLayout(form)


        self.gdoc_sheet_uri_ui = QLineEdit()
        form.addRow("GSheet URI", self.gdoc_sheet_uri_ui)

        self.gdoc_sheet_id_ui = QLineEdit()
        self.gdoc_sheet_id_ui.setFixedWidth(50)
        self.gdoc_sheet_id_ui.setValidator(QIntValidator(0, 10000))
        form.addRow("GSheet Id", self.gdoc_sheet_id_ui)

        self.hwind_achash = QLineEdit()
        form.addRow("Purge Account Hash:", self.hwind_achash)

        self.hwind_token = QLineEdit()
        form.addRow("Purge Token: ", self.hwind_token)

        if settingdict != None:
            self.gdoc_sheet_uri_ui.setText(settingdict["settings"]["gdocURI"])
            self.gdoc_sheet_id_ui.setText(settingdict["settings"]["gsheetId"])

            self.hwind_achash.setText(settingdict["settings"]["hwind"]["achash"])
            self.hwind_token.setText(settingdict["settings"]["hwind"]["token"])

        self.apply_button = QPushButton("Apply", self)
        self.apply_button.clicked.connect(self.SaveSettings)
        form.addWidget(self.apply_button)

    def keyPressEvent(self, event):
        if event.key() == QtCore.Qt.Key_Escape:
            self.close()

    def SaveSettings(self):

        xml_settings = {"gdocURI" : str(self.gdoc_sheet_uri_ui.text()), "gsheetId" : str(self.gdoc_sheet_id_ui.text()),
                         "hwind" : {"achash" : str(self.hwind_achash.text()), "token" : str(self.hwind_token.text())}}

        try:
            xml = dicttoxml.dicttoxml(xml_settings,attr_type=False,custom_root="settings")

            with open('settings.xml', mode="w") as f:
                f.write(xml)

        except:
            raise