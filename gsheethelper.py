#src http://stackoverflow.com/questions/3287651/download-a-spreadsheet-from-google-docs-using-python
#thanks to Cameron Spickert

import requests

class GSheet():

    @staticmethod
    def GetSheet(sheet_id,gid=0,format="csv"):
        request = 'https://docs.google.com/spreadsheet/ccc?key=%s&gid=%i&output=%s' % (sheet_id, gid, format)
        response = requests.get(request)
        assert response.status_code == 200, 'Wrong status code'
        return response.content
