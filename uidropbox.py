import os
from PyQt4 import QtGui, QtCore

from PyQt4.QtCore import QString


class UIDropbox(QtGui.QPushButton):
    required_file = None
    selected_file = None

    def __init__(self, title, parent):
       super(UIDropbox, self).__init__(parent)
       self.setText("Drag and drop file here")
       self.setIcon(QtGui.QIcon("res/text-document-icon.png"))
       self.setIconSize(QtCore.QSize(100, 100))
       self.setMinimumSize(QtCore.QSize(100, 100))
       self.setStyleSheet("QPushButton { background-color: rgb(225, 225,\
       225); border:3px solid rgb(100, 100, 100); }")
       self.setAcceptDrops(True)

    def SetRequiredFile(self,filename):
        self.required_file=filename
        print "Required file name set to: " +self.required_file

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls() and os.path.isfile(e.mimeData().urls()[0].toString(QtCore.QUrl.RemoveScheme)):
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        self.selected_file = str(e.mimeData().urls()[0].toString(QtCore.QUrl.RemoveScheme))
        self.UpdateUI()

    def UpdateUI(self):

        if self.selected_file == None or  self.required_file == None:
            return

        display_text = os.path.basename(self.selected_file)
        if display_text != self.required_file:
            display_text = display_text + "\n[File name mismatch]"
            self.setIcon(QtGui.QIcon("res/exclamation-mark.png"))
            self.setIconSize(QtCore.QSize(35, 35))
            self.setStyleSheet("QPushButton { font-weight: bold; background-color: rgb(225, 75,\
                    75); border:3px solid rgb(0, 0, 0); }")
        else:
            self.setIcon(QtGui.QIcon("res/tick-mark.png"))
            self.setIconSize(QtCore.QSize(50, 50))
            self.setStyleSheet("QPushButton { background-color: rgb(255, 255,\
                    255); border:3px solid rgb(100, 100, 100); }")

        self.setText(display_text)

    def DoesFileNamesMatch(self):
        return self.selected_file != None and os.path.basename(self.selected_file) == self.required_file


